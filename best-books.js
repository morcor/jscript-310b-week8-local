const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

formEl.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;

  const BASE_URL = `https://api.nytimes.com/svc/books/v3/lists/${year}-${month}-${date}/hardcover-fiction.json`;

console.log(BASE_URL);

const url = `${BASE_URL}?api-key=${API_KEY}`;

fetch(url)
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    const bookList = responseJson.results.books;
    console.log(bookList);

    let book = responseJson.results.books[0];
    console.log(book);

    const bookTitle = book.title;
    console.log(bookTitle);

    const bookAuthor = book.author;
    console.log(bookAuthor);

    const book_Image = book.book_image;
    

    const bookDescription = book.description;
    console.log(bookDescription);

    let completeList = "";

    const getBookDetail = function(book)  {
      return `<img src="${book.book_image}" alt="${book.title}"><br>Title: ${book.title}<br><br> Description: ${book.description}<br><br> By Author: ${book.author}<br><br><hr>`;

    }

    for (let i = 0; i < 5; i++) {
      let book = responseJson.results.books[i];
      completeList += getBookDetail(book);
    }
    document.getElementById("books-container").innerHTML = completeList;

  });

  // Fetch bestselling books for date and add top 5 to page
});












const myPromise = new Promise(function (resolve, reject) {
  setTimeout(function () {
    if (Math.random() >= 0.5) {
      resolve();
    } else {
      reject();
    }
    }, 1000);
  });

  myPromise.then(() => {
    console.log("Success!");
  })
  .catch(() => {
    console.log("fail");
  })
  .then(() => {
    console.log("complete");
  });




 
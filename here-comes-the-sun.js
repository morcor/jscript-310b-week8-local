document.addEventListener("DOMContentLoaded", () => {
  let body = document.querySelector("body");
  let colorValue = 000;
  const colorChangeInterval = setInterval(function () {
    if (colorValue < 000) {
      requestAnimationFrame(colorChangeInterval);
    }
    colorValue += 5;
    body.style.backgroundColor = `rgb(${colorValue}, ${colorValue}, ${colorValue})`;
  }, 30);
});
